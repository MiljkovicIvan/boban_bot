#include <ServoTimer2.h>
#include <NewPing.h>


#define trg 4
#define echo 13
#define pwm1 6
#define m1a 7
#define m1b 8
#define pwm2 9
#define m2a 10
#define m2b 11
#define ser 3
#define spd 120
#define pravo 1400
#define levo_skroz 700
#define desno_skroz 2100
#define desno_polu 1750
#define levo_polu 1050


NewPing sonar(trg, echo, 400);
ServoTimer2 servo;



void halt()
{
  digitalWrite(pwm1, LOW);
  digitalWrite(pwm2, LOW);
}

void napred()
{
  analogWrite(pwm1, spd);
  analogWrite(pwm2, spd);
  digitalWrite(m1a, HIGH);
  digitalWrite(m2b, HIGH);
  digitalWrite(m1b, LOW);
  digitalWrite(m2a, LOW);
}


void nazad()
{
  analogWrite(pwm1, spd);
  analogWrite(pwm2, spd);
  digitalWrite(m1b, HIGH);
  digitalWrite(m2a, HIGH);
  digitalWrite(m1a, LOW);
  digitalWrite(m2b, LOW);
}

void levo()
{
  analogWrite(pwm1, spd);
  analogWrite(pwm2, spd);
  digitalWrite(m2a, LOW);
  digitalWrite(m2b, HIGH);
  digitalWrite(m1a, LOW);
  digitalWrite(m1b, HIGH);
}

void desno()
{
  analogWrite(pwm1, spd);
  analogWrite(pwm2, spd);
  digitalWrite(m2b, LOW);
  digitalWrite(m2a, HIGH);
  digitalWrite(m1b, LOW);
  digitalWrite(m1a, HIGH);
}

int prioritet(int a){

    int ret;

    if((a == 0) || (a == 4)) ret = 1;
    else if ((a == 1) || (a == 3)) ret = 2;
    else if (a == 2) ret = 3;
    else ret = 0;
    return ret;
}

int pera(int a[]){

    int ret;
	int br = 0, b[5]={0};
	int max = prioritet(b[0]);
    int min = a[0];
    int i;
	int provera = 0;
    for (i = 0; i<5; i++)
        if (min <= a[i])
            min = a[i];

    
    //printf("min je %d\n", min);  //proveravam MIN
    for (i = 0; i<5; i++)
        if (min == a[i])
            provera++;

    //printf("provera je %d\n", provera);  // proveravam proveru

    if (provera > 1){
        
        for (i = 0; i<5; i++)
        if (min == a[i]){
            b[br] = i;
            br++;
        }



    
        for (i = 0; i<br; i++){
            if (max <= prioritet(b[i])){
                max = prioritet(b[i]);
                ret = b[i];
            }
        }
    }
    else {

        // vrati index minimuma
        for (i = 0; i<5; i++)
            if (min == a[i])
                ret = i;
    }

    //printf("ret je %d\n", ret);
    return ret;

}

int skeniraj(){   // da pinguje 10 puta i da nadje prosek, onako je dosta nepracizan
  
  int dst = 0;
  int i;
  for (i =0; i<5; i++){
  dst += sonar.ping_cm();
  delay(30);
  }
  
  dst = dst / 5;
  
  return dst;
  
}


int pogledaj(){  //ideja je da stane, pogleda gde mi je najdalje, i tamo da pici
                // ne prosledjuje se nista, beta faza
   halt();
   delay(100); 
   int a[5], dst1, dst2;
   
   servo.attach(ser);
   
   
   
   servo.write(levo_skroz);
   delay(300);

   a[0] = skeniraj();
  // if(a[0] > 30) a[0] = 30;
   
   
   servo.write(levo_polu);
   delay(300);
 a[1] = skeniraj();
   //if(a[1] > 30) a[1] = 30;
   
   
   servo.write(pravo);
   delay(300);
   a[2] = skeniraj();
   //if(a[2] > 30) a[2] = 30;
   
   
   servo.write(desno_polu);
   delay(300);
  a[3] = skeniraj();
   //if(a[3] > 30) a[3] = 30;

   
   servo.write(desno_skroz);
   delay(300);
 a[4] = skeniraj();
   //if(a[4] > 30) a[4] = 30;
   
   
   servo.write(pravo);
   delay(300);
   servo.detach();
   
   int pravac = pera(a);
   
   return pravac;
   
}

void skreni(int pravac){
 
   switch (pravac){
    case 0 : levo(); delay(350); halt(); break;
    case 1 : levo(); delay(250); halt(); break;
    case 2 : napred(); break;
    case 3 : desno(); delay(250); halt(); break;
    case 4 : desno(); delay(350); halt(); break;
   } 
  napred();
}
//int bul = 1, pravac = 1;

void setup() {
  pinMode(pwm1, OUTPUT);
  pinMode(pwm2, OUTPUT);
  pinMode(m1a, OUTPUT);
  pinMode(m1b, OUTPUT);
  pinMode(m2a, OUTPUT);
  pinMode(m2b, OUTPUT);
  
  servo.attach(ser);
  servo.write(pravo);
  delay(500);
  servo.detach();
}

void loop(){
    
   delay(50);
   int dst1 = sonar.ping_cm();
   delay(50);
   int dst2 = sonar.ping_cm();
   delay(50);
   int dst = (dst1 + dst2) /2;
   if (dst < 18){
    skreni(pogledaj());

 }
   else {
     napred(); 
 
 }
}



